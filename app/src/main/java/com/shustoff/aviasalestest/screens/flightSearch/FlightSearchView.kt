package com.shustoff.aviasalestest.screens.flightSearch

import android.content.res.Resources
import com.shustoff.aviasalestest.R
import com.shustoff.aviasalestest.model.City
import com.shustoff.mvp.MvpView
import com.shustoff.mvp.ViewStateEmitter
import com.shustoff.mvp.inflater.RootViewInflater
import com.shustoff.mvp.render.renderer
import kotlinx.android.synthetic.main.screen_flight_search.view.*
import javax.inject.Inject

class FlightSearchView @Inject constructor(
    stateEmitter: ViewStateEmitter<FlightSearchState>,
    inflater: RootViewInflater,
    private val resources: Resources
) : MvpView<FlightSearchState>(stateEmitter) {

    private val view = inflater.inflateRootView(R.layout.screen_flight_search)

    private val destination = renderer<City?> {
        view.destination.text = it?.name
            ?: resources.getString(R.string.to)
    }

    private val origin = renderer<City?> {
        view.from.text = it?.name
            ?: resources.getString(R.string.from)
    }

    private val search = renderer<(() -> Unit)?> {
        view.search.isEnabled = it != null
    }

    init {
        view.from.setOnClickListener {
            state?.chooseStart?.invoke()
        }
        view.destination?.setOnClickListener {
            state?.chooseDestination?.invoke()
        }
        view.search?.setOnClickListener {
            state?.search?.invoke()
        }
    }

    override fun onRenderState(state: FlightSearchState) {
        destination.render(state.destination)
        origin.render(state.from)
        search.render(state.search)
    }
}