package com.shustoff.aviasalestest.screens.flightMap

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PathMeasure
import android.graphics.Point
import android.graphics.RectF
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.doOnNextLayout
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.Projection
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Dot
import com.google.android.gms.maps.model.Gap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.shustoff.aviasalestest.R
import com.shustoff.aviasalestest.model.CurvePathCreator
import com.shustoff.mvp.di.annotations.MvpViewSingleton
import javax.inject.Inject
import kotlin.math.atan2
import kotlin.math.max
import kotlin.math.min

@MvpViewSingleton
class FlightMap @Inject constructor(
    rootView: View,
    private val pathCreator: CurvePathCreator,
    mapFragment: SupportMapFragment
) {

    private val context = rootView.context
    private val resources = rootView.resources
    private val mapCameraPadding = resources.getDimensionPixelOffset(R.dimen.four_paddings)

    private var map: GoogleMap? = null

    private var scene: Scene? = null
    private var path: FlightMapState.Path? = null
    private var lastRenderedPath: FlightMapState.Path? = null
    private var onLayoutCalled = false
    private var progress: Float? = null

    init {
        mapFragment.view?.doOnNextLayout {
            onLayoutCalled = true
            path?.also(::renderPath)
        }
        mapFragment.getMapAsync { googleMap ->
            map = googleMap
            path?.also(::renderPath)
        }
    }

    fun renderPath(path: FlightMapState.Path) {
        this.path = path
        map.takeIf { onLayoutCalled && lastRenderedPath != path }
            ?.also { map ->
                lastRenderedPath = path
                renderPath(path, map)
            }
    }

    fun updateProgress(progress: Float) {
        this.progress = progress

        scene?.also { (measure, projection, flightMarker) ->
            val tan = FloatArray(2)
            val pos = FloatArray(2)
            val distance = progress * measure.length
            measure.getPosTan(distance, pos, tan)
            flightMarker.position = projection.fromScreenLocation(
                Point(pos[0].toInt(), pos[1].toInt())
            )
            flightMarker.rotation = Math.toDegrees(
                atan2(tan[1].toDouble(), tan[0].toDouble())
            ).toFloat()
        }
    }

    private fun renderPath(
        path: FlightMapState.Path,
        map: GoogleMap
    ) {
        map.clear()
        val from = LatLng(path.from.location.lat, path.from.location.lon)
        val to = LatLng(path.to.location.lat, path.to.location.lon)

        map.addMarker(
            createCityMarker(from, path.from.shortName)
        )
        map.addMarker(
            createCityMarker(to, path.to.shortName)
        )

        val minLon = min(from.longitude, to.longitude)
        val maxLon = max(from.longitude, to.longitude)
        val inverseLon = maxLon - minLon > 180
        map.moveCamera(
            CameraUpdateFactory.newLatLngBounds(
                LatLngBounds(
                    LatLng(
                        min(from.latitude, to.latitude),
                        if (inverseLon) maxLon else minLon
                    ),
                    LatLng(
                        max(from.latitude, to.latitude),
                        if (inverseLon) minLon else maxLon
                    )
                ),
                mapCameraPadding
            )
        )

        val flightMarker = map.addMarker(
            createFlightMarker(from)
        )

        val projection = map.projection

        val measure = pathCreator.makePath(
            projection.toScreenLocation(from),
            projection.toScreenLocation(to)
        )

        val polyLineOptions = createPathPolyline(measure, projection)
        val polyline = map.addPolyline(polyLineOptions)
        polyline.pattern = listOf(Dot(), Gap(10f))

        scene = Scene(
            measure = measure,
            projection = projection,
            flightMarker = flightMarker
        )
    }

    private fun createFlightMarker(from: LatLng): MarkerOptions {
        return MarkerOptions()
            .position(from)
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_plane))
            .anchor(0.5f, 0.5f)
            .zIndex(1f)
    }

    private fun createCityMarker(latLng: LatLng, name: String): MarkerOptions {
        val textPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        textPaint.textSize = resources.getDimensionPixelSize(R.dimen.city_label_text_size).toFloat()
        textPaint.color = Color.BLACK
        textPaint.textAlign = Paint.Align.LEFT

        val verticalPadding = resources.getDimensionPixelOffset(R.dimen.half_padding)
        val horizontalPadding = resources.getDimensionPixelOffset(R.dimen.one_padding)

        val baseline = -textPaint.ascent()
        val width = (textPaint.measureText(name) + horizontalPadding * 2).toInt()
        val height = (baseline + textPaint.descent() + verticalPadding * 2).toInt()
        val image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(image)

        val backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        backgroundPaint.color = ContextCompat.getColor(context, R.color.colorAccent)
        canvas.drawRoundRect(
            RectF(0f, 0f, width.toFloat(), height.toFloat()),
            height / 2f,
            height / 2f,
            backgroundPaint
        )
        canvas.drawText(name, horizontalPadding.toFloat(), baseline + verticalPadding, textPaint)

        return MarkerOptions()
            .position(latLng)
            .icon(BitmapDescriptorFactory.fromBitmap(image))
            .anchor(0.5f, 0.5f)
    }

    private fun createPathPolyline(
        pathMeasure: PathMeasure,
        projection: Projection
    ): PolylineOptions {
        val sourcePoints = (0..curvePointsCount)
            .map { i ->
                val pos = FloatArray(2)
                val distance = pathMeasure.length / curvePointsCount * i
                pathMeasure.getPosTan(distance, pos, null)
                projection.fromScreenLocation(Point(pos[0].toInt(), pos[1].toInt()))
            }

        val polyLineOptions = PolylineOptions()
        polyLineOptions.addAll(sourcePoints)
        polyLineOptions.width(10f)
        polyLineOptions.color(Color.GRAY)
        return polyLineOptions
    }

    private data class Scene(
        val measure: PathMeasure,
        val projection: Projection,
        val flightMarker: Marker
    )

    companion object {

        private const val curvePointsCount = 100
    }
}