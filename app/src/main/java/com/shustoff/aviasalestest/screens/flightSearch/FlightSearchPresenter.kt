package com.shustoff.aviasalestest.screens.flightSearch

import com.shustoff.aviasalestest.model.City
import com.shustoff.aviasalestest.model.FlightSearchPreferences
import com.shustoff.aviasalestest.navigation.Router
import com.shustoff.aviasalestest.navigation.Screen
import com.shustoff.aviasalestest.navigation.Screen.CitySearch.Type
import com.shustoff.mvp.MvpPresenter
import kotlinx.coroutines.launch
import javax.inject.Inject

class FlightSearchPresenter @Inject constructor(
    private val searchPreferences: FlightSearchPreferences,
    private val router: Router
) : MvpPresenter<FlightSearchState>() {

    private var state: FlightSearchState

    init {
        val from = searchPreferences.getStartCity()
        val destination = searchPreferences.getDestinationCity()

        state = FlightSearchState(
            from = from,
            destination = destination,
            chooseDestination = ::chooseDestination,
            chooseStart = ::chooseStart,
            search = ::startSearch.takeIf { from != null && destination != null }
        )
        renderState(state)
    }

    private fun startSearch() {
        state.also {
            router.goTo(
                Screen.FlightMap(
                    from = it.from ?: return,
                    to = it.destination ?: return
                )
            )
        }
    }

    private fun chooseStart() {
        launch {
            val city = router.goAndWaitForResult(Screen.CitySearch(Type.FROM)) as? City
            if (city != null) {
                searchPreferences.saveStartCity(city)
                state = state.let { state ->
                    state.copy(
                        from = city,
                        search = ::startSearch
                            .takeIf { state.destination != null && state.destination != city }
                    )
                }
                renderState(state)
            }
        }
    }

    private fun chooseDestination() {
        launch {
            val city = router.goAndWaitForResult(Screen.CitySearch(Type.TO)) as? City
            if (city != null) {
                searchPreferences.saveDestinationCity(city)
                state = state.let { state ->
                    state.copy(
                        destination = city,
                        search = ::startSearch.takeIf { state.from != null && state.from != city }
                    )
                }
                renderState(state)
            }
        }
    }
}