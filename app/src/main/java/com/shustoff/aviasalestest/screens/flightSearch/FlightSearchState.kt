package com.shustoff.aviasalestest.screens.flightSearch

import com.shustoff.aviasalestest.model.City

data class FlightSearchState(
    val from: City?,
    val destination: City?,
    val chooseStart: () -> Unit,
    val chooseDestination: () -> Unit,
    val search: (() -> Unit)?
)