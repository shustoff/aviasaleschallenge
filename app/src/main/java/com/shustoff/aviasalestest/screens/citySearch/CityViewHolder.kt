package com.shustoff.aviasalestest.screens.citySearch

import android.view.View
import com.shustoff.aviasalestest.model.City
import com.shustoff.diffadapter.TypedViewHolder
import kotlinx.android.synthetic.main.item_city.view.*

class CityViewHolder(
    itemView: View,
    onItemClick: (City) -> Unit
) : TypedViewHolder<City>(itemView, onItemClick) {

    override fun bind(item: City) {
        itemView.label.text = item.shortName
        itemView.name.text = item.name
    }
}