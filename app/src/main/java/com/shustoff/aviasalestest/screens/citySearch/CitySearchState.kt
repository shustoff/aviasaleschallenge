package com.shustoff.aviasalestest.screens.citySearch

import com.shustoff.aviasalestest.model.City
import com.shustoff.aviasalestest.navigation.Screen

data class CitySearchState(
    val query: String,
    val searchType: Screen.CitySearch.Type,
    val onQueryChanged: (String) -> Unit,
    val cities: List<City>,
    val onCityClick: (City) -> Unit,
    val serverError: Boolean
)