package com.shustoff.aviasalestest.screens.citySearch

import com.shustoff.aviasalestest.model.City
import com.shustoff.aviasalestest.navigation.Router
import com.shustoff.aviasalestest.navigation.Screen
import com.shustoff.mvp.MvpPresenter
import com.shustoff.mvp.navigation.ScreenInfo
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import javax.inject.Inject

class CitySearchPresenter @Inject constructor(
    private val interactor: CitySearchInteractor,
    private val screen: ScreenInfo<Screen.CitySearch>,
    private val router: Router
) : MvpPresenter<CitySearchState>() {

    private var state = CitySearchState(
        query = "",
        onQueryChanged = ::onQueryChanged,
        cities = emptyList(),
        onCityClick = ::onCityClick,
        serverError = false,
        searchType = screen.screen.type
    )

    private val queryChannel = Channel<String>()

    init {
        renderState(state)
        launch {
            interactor.search(queryChannel) { result ->
                state.takeIf { it.query == result.query }
                    ?.copy(
                        cities = result.cities,
                        serverError = result.hasServerError
                    )
                    ?.also {
                        state = it
                        renderState(it)
                    }
            }
        }
    }

    override fun onDestroyed() {
        super.onDestroyed()
        queryChannel.cancel()
    }

    private fun onCityClick(city: City) {
        router.postResultAndClose(screen.resultHandle, city)
    }

    private fun onQueryChanged(query: String) {
        state = state.copy(query = query)
        queryChannel.offer(query)
    }
}