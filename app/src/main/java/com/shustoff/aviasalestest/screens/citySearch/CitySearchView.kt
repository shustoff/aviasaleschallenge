package com.shustoff.aviasalestest.screens.citySearch

import android.content.res.Resources
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import com.shustoff.aviasalestest.R
import com.shustoff.aviasalestest.model.City
import com.shustoff.aviasalestest.navigation.Screen
import com.shustoff.diffadapter.diffAdapter
import com.shustoff.mvp.MvpView
import com.shustoff.mvp.ViewStateEmitter
import com.shustoff.mvp.inflater.RootViewInflater
import com.shustoff.mvp.render.renderer
import kotlinx.android.synthetic.main.screen_city_search.view.*
import javax.inject.Inject

class CitySearchView @Inject constructor(
    stateEmitter: ViewStateEmitter<CitySearchState>,
    inflater: RootViewInflater,
    private val resources: Resources
) : MvpView<CitySearchState>(stateEmitter) {

    private val view = inflater.inflateRootView(R.layout.screen_city_search)

    private var ignoreQueryChanges = false

    private val adapter = diffAdapter {
        register(R.layout.item_city) {
            CityViewHolder(it, ::onCityClick)
        }
    }

    private val hint = renderer<Screen.CitySearch.Type> {
        view.query.hint = when (it) {
            Screen.CitySearch.Type.FROM -> resources.getString(R.string.from)
            Screen.CitySearch.Type.TO -> resources.getString(R.string.to)
        }
    }

    private val error = renderer<Boolean> {
        view.error.isVisible = it
    }

    init {
        view.cities.adapter = adapter
        view.query.doOnTextChanged { text, _, _, _ ->
            state?.onQueryChanged
                ?.takeUnless { ignoreQueryChanges }
                ?.invoke(text?.toString().orEmpty())
        }
    }

    private fun onCityClick(city: City) {
        state?.onCityClick?.invoke(city)
    }

    override fun onRenderState(state: CitySearchState) {
        hint.render(state.searchType)
        error.render(state.serverError)
        if (state.query != view.query.text?.toString()) {
            ignoreQueryChanges = true
            view.query.setText(state.query)
            view.query.setSelection(state.query.length)
            ignoreQueryChanges = false
        }
        adapter.show(state.cities)
    }
}