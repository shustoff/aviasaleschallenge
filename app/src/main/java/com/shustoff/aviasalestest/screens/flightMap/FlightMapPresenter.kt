package com.shustoff.aviasalestest.screens.flightMap

import com.shustoff.aviasalestest.navigation.Screen
import com.shustoff.mvp.MvpPresenter
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.delayEach
import kotlinx.coroutines.launch
import javax.inject.Inject

class FlightMapPresenter @Inject constructor(
    screen: Screen.FlightMap
) : MvpPresenter<FlightMapState>() {

    private val path = FlightMapState.Path(
        from = screen.from,
        to = screen.to
    )

    init {
        updateState(0f)
        launch {
            (1..stepsCount).asFlow()
                .delayEach(500)
                .collect {
                    updateState(it / stepsCount.toFloat())
                }
        }
    }

    private fun updateState(progress: Float) {
        renderState(
            FlightMapState(
                path = path,
                progress = progress
            )
        )
    }

    companion object {
        private const val stepsCount = 50
    }
}