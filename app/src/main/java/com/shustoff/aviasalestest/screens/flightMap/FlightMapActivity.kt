package com.shustoff.aviasalestest.screens.flightMap

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shustoff.aviasalestest.navigation.Screen
import com.shustoff.mvp.di.module
import com.shustoff.mvp.di.viewModule
import com.shustoff.mvp.mvp
import com.shustoff.mvp.navigation.ScreenActivity

class FlightMapActivity : AppCompatActivity(), ScreenActivity<Screen.FlightMap> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mvp<FlightMapPresenter, FlightMapView>(
            module {
                instance(screen)
            }
        ).onViewCreated(viewModule())
    }
}