package com.shustoff.aviasalestest.screens.citySearch

import com.shustoff.aviasalestest.extensions.asFlow
import com.shustoff.aviasalestest.extensions.singleAsFlow
import com.shustoff.aviasalestest.model.CitiesRepository
import com.shustoff.aviasalestest.model.City
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.switchMap
import javax.inject.Inject

class CitySearchInteractor @Inject constructor(
    private val repository: CitiesRepository
) {

    suspend fun search(
        queryChannel: Channel<String>,
        handleResult: suspend (Result) -> Unit
    ) {
        queryChannel.asFlow()
            .debounce(500)
            .switchMap { query ->
                singleAsFlow {
                    val cities = runCatching { repository.searchForCity(query) }.getOrNull()
                    Result(
                        cities = cities.orEmpty(),
                        hasServerError = cities == null,
                        query = query
                    )
                }
            }
            .collect(handleResult)
    }

    data class Result(
        val query: String,
        val cities: List<City>,
        val hasServerError: Boolean
    )
}