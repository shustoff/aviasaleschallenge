package com.shustoff.aviasalestest.screens.citySearch

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shustoff.aviasalestest.navigation.Screen
import com.shustoff.mvp.di.module
import com.shustoff.mvp.di.viewModule
import com.shustoff.mvp.mvp
import com.shustoff.mvp.navigation.ScreenActivity

class CitySearchActivity : AppCompatActivity(), ScreenActivity<Screen.CitySearch> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mvp<CitySearchPresenter, CitySearchView>(
            module {
                instance(screenInfo)
            }
        ).onViewCreated(viewModule())
    }
}