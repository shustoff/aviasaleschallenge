package com.shustoff.aviasalestest.screens.flightMap

import com.shustoff.aviasalestest.model.City

data class FlightMapState(
    val path: Path,
    val progress: Float
) {

    data class Path(
        val from: City,
        val to: City
    )
}