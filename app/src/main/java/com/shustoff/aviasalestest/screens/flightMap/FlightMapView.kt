package com.shustoff.aviasalestest.screens.flightMap

import android.animation.ValueAnimator
import android.view.animation.LinearInterpolator
import androidx.fragment.app.FragmentManager
import com.google.android.gms.maps.SupportMapFragment
import com.shustoff.aviasalestest.R
import com.shustoff.aviasalestest.model.CurvePathCreator
import com.shustoff.mvp.MvpView
import com.shustoff.mvp.ViewStateEmitter
import com.shustoff.mvp.inflater.RootViewInflater
import javax.inject.Inject

class FlightMapView @Inject constructor(
    stateEmitter: ViewStateEmitter<FlightMapState>,
    inflater: RootViewInflater,
    fragmentManager: FragmentManager,
    pathCreator: CurvePathCreator
) : MvpView<FlightMapState>(stateEmitter) {

    private val flightMap = FlightMap(
        rootView = inflater.inflateRootView(R.layout.screen_map),
        mapFragment = fragmentManager.findFragmentById(R.id.map) as SupportMapFragment,
        pathCreator = pathCreator
    )

    private var currentAnimator: ValueAnimator? = null

    private var lastRenderedProgress = 0f

    override fun onRenderState(state: FlightMapState) {
        flightMap.renderPath(state.path)
        currentAnimator?.cancel()
        currentAnimator = ValueAnimator.ofFloat(
            lastRenderedProgress,
            state.progress
        ).also { animator ->
            animator.addUpdateListener {
                val progress = animator.animatedValue as Float
                lastRenderedProgress = progress
                flightMap.updateProgress(progress)
            }
            animator.duration = 500
            animator.interpolator = LinearInterpolator()
            animator.start()
        }
    }

    override fun onDestroyed() {
        super.onDestroyed()
        currentAnimator?.cancel()
    }
}