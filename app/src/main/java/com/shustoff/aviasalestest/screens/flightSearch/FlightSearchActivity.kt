package com.shustoff.aviasalestest.screens.flightSearch

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shustoff.mvp.di.viewModule
import com.shustoff.mvp.mvp

class FlightSearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mvp<FlightSearchPresenter, FlightSearchView>()
            .onViewCreated(viewModule())
    }
}
