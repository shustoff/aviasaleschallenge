package com.shustoff.aviasalestest.model

import java.io.Serializable

data class Location(
    val lat: Double,
    val lon: Double
) : Serializable