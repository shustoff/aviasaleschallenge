package com.shustoff.aviasalestest.model

import android.graphics.Path
import android.graphics.PathMeasure
import android.graphics.Point
import android.graphics.PointF
import javax.inject.Inject

class CurvePathCreator @Inject constructor() {

    fun makePath(startPoint: Point, endPoint: Point): PathMeasure {
        val path = Path()
        path.moveTo(startPoint.x.toFloat(), startPoint.y.toFloat())

        val (firstControlPoint, secondControlPoint) = makeControlPoints(startPoint, endPoint)
        path.cubicTo(
            firstControlPoint.x,
            firstControlPoint.y,
            secondControlPoint.x,
            secondControlPoint.y,
            endPoint.x.toFloat(),
            endPoint.y.toFloat()
        )

        return PathMeasure(path, false)
    }

    private fun makeControlPoints(startPoint: Point, endPoint: Point): Pair<PointF, PointF> {
        val distance = PointF(
            endPoint.x.toFloat() - startPoint.x.toFloat(),
            endPoint.y.toFloat() - startPoint.y.toFloat()
        )
        val middle = PointF(
            startPoint.x + distance.x / 2,
            startPoint.y + distance.y / 2
        )
        val first = PointF(
            middle.x - distance.y / 2,
            middle.y + distance.x / 2
        )
        val second = PointF(
            middle.x + distance.y / 2,
            middle.y - distance.x / 2
        )

        return first to second
    }

}