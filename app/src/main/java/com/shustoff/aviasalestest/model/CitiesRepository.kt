package com.shustoff.aviasalestest.model

import com.shustoff.aviasalestest.api.CitiesApi
import java.util.Locale
import javax.inject.Inject

class CitiesRepository @Inject constructor(
    private val api: CitiesApi
) {

    suspend fun searchForCity(query: String) = api
        .autocomplete(
            term = query,
            lang = Locale.getDefault().language
        )
        .cities
        .map {
            City(
                name = it.fullname,
                location = Location(
                    lat = it.location.lat,
                    lon = it.location.lon
                ),
                id = it.id,
                shortName = it.iata.firstOrNull() ?: it.city
            )
        }
}