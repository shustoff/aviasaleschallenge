package com.shustoff.aviasalestest.model

import com.shustoff.diffadapter.WithId
import java.io.Serializable

data class City(
    override val id: Int,
    val name: String,
    val location: Location,
    val shortName: String
) : Serializable, WithId<Int>