package com.shustoff.aviasalestest.model

import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FlightSearchPreferences @Inject constructor(
    private val preferences: SharedPreferences
) {

    private val gson = Gson()

    fun getStartCity(): City? = getCity(startCityKey)

    fun getDestinationCity(): City? = getCity(destinationCityKey)

    fun saveStartCity(city: City) {
        saveCity(city, startCityKey)
    }

    fun saveDestinationCity(city: City) {
        saveCity(city, destinationCityKey)
    }

    private fun saveCity(city: City, key: String) {
        preferences.edit {
            putString(key, gson.toJson(city))
        }
    }

    private fun getCity(key: String): City? {
        return preferences.getString(key, null)
            ?.let {
                gson.fromJson(it, City::class.java)
            }
    }

    companion object {

        private const val startCityKey = "startCityKey"
        private const val destinationCityKey = "destinationCityKey"
    }
}