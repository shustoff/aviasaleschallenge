package com.shustoff.aviasalestest.extensions

import kotlinx.coroutines.flow.flow

fun <T> singleAsFlow(
    single: suspend () -> T
) = flow {
    emit(single.invoke())
}