package com.shustoff.aviasalestest.extensions

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.flow

fun <T> Channel<T>.asFlow() = flow {
    for (query in this@asFlow) {
        emit(query)
    }
}