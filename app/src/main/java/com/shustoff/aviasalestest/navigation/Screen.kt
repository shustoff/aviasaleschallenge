package com.shustoff.aviasalestest.navigation

import com.shustoff.aviasalestest.model.City
import java.io.Serializable

sealed class Screen : Serializable {

    data class FlightMap(
        val from: City,
        val to: City
    ) : Screen()

    data class CitySearch(
        val type: Type
    ) : Screen() {

        enum class Type {
            FROM,
            TO
        }
    }

}