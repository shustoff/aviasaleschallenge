package com.shustoff.aviasalestest.navigation

import android.app.Application
import com.shustoff.aviasalestest.screens.citySearch.CitySearchActivity
import com.shustoff.aviasalestest.screens.flightMap.FlightMapActivity
import com.shustoff.mvp.navigation.RouterBase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Router @Inject constructor(
    application: Application
) : RouterBase<Screen>(application) {

    override fun ScreenLaunchingContext<Screen>.goToScreen(screen: Screen) {
        when (screen) {
            is Screen.CitySearch -> startActivity(CitySearchActivity::class, screen)
            is Screen.FlightMap -> startActivity(FlightMapActivity::class, screen)
        }
    }
}