package com.shustoff.aviasalestest

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import com.shustoff.aviasalestest.api.CitiesApi
import com.shustoff.aviasalestest.api.CitiesApiProvider
import com.shustoff.aviasalestest.api.RetrofitProvider
import com.shustoff.mvp.di.Di
import com.shustoff.mvp.di.module
import retrofit2.Retrofit

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Di.init(this,
            module {
                instance<Application>(this@App)
                instance<Context>(this@App)
                instance<Resources>(resources)
                instance<SharedPreferences>(
                    getSharedPreferences("app_preferences", Context.MODE_PRIVATE)
                )
                provided<Retrofit>(singleton = true).by<RetrofitProvider>()
                provided<CitiesApi>(singleton = true).by<CitiesApiProvider>()
            }
        )
    }
}