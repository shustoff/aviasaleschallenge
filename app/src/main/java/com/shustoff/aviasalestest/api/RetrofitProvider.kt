package com.shustoff.aviasalestest.api

import android.content.res.Resources
import com.shustoff.aviasalestest.R
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class RetrofitProvider @Inject constructor(
    private val resources: Resources
) : Provider<Retrofit> {

    override fun get(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(resources.getString(R.string.server_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}