package com.shustoff.aviasalestest.api.dto

class CitiesResponseDto(
    val cities: List<CityDto>
)