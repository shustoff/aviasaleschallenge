package com.shustoff.aviasalestest.api.dto

class CityDto(
    val id: Int,
    val fullname: String,
    val location: LocationDto,
    val iata: List<String>,
    val city: String
)