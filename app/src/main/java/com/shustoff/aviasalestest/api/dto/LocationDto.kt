package com.shustoff.aviasalestest.api.dto

class LocationDto(
    val lat: Double,
    val lon: Double
)