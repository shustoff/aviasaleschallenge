package com.shustoff.aviasalestest.api

import retrofit2.Retrofit
import retrofit2.create
import javax.inject.Inject
import javax.inject.Provider

class CitiesApiProvider @Inject constructor(
    private val retrofit: Retrofit
) : Provider<CitiesApi> {

    override fun get(): CitiesApi = retrofit.create()
}