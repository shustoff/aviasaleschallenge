package com.shustoff.aviasalestest.api

import com.shustoff.aviasalestest.api.dto.CitiesResponseDto
import retrofit2.http.GET
import retrofit2.http.Query

interface CitiesApi {

    @GET("/autocomplete")
    suspend fun autocomplete(
        @Query("term") term: String,
        @Query("lang") lang: String
    ) : CitiesResponseDto
}